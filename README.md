# Ruby - Sinatra

## Description

This repository its a hello world server taken from [this GitGub](https://github.com/sinatra/sinatra)

## Before run you must intall

Gem 

You can use:
```shell
apt install gem
```
[Ruby](https://www.ruby-lang.org/es/documentation/installation/)

[Sinatra](https://www.digitalocean.com/community/tutorials/how-to-install-and-get-started-with-sinatra-on-your-system-or-vps)

## To run:
```shell
ruby app.rb
```
